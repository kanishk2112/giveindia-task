var elem = document.querySelector('.js-switch');
var init = new Switchery(elem);

$("#name").focus();

var changeCheckbox = document.querySelector('.js-switch');

changeCheckbox.onchange = function() {
	if(changeCheckbox.checked) {
		$(".isIndianNo").removeClass('text-success');
		$(".isIndianYes").addClass('text-success');
	}
	else {
		$(".isIndianYes").removeClass('text-success');
		$(".isIndianNo").addClass('text-success');
	}
};

$(".btnAmount").on('click', function(event) {
	event.preventDefault();
	$("#amount").val($(this).html());
	clearAllValidations();
});

$("#btnSubmit").on('click', function(event) {
	event.preventDefault();
	var isFormValid = validateForm();
	if(isFormValid) {
		//proceed with form submission

		var finalData = $("#mainForm").serializeArray();
		finalData.push({
			name: "isIndian",
			value: changeCheckbox.checked
		});

		$.ajax({
			url: 'mailer/mailer.php',
			type: 'POST',
			dataType: 'JSON',
			data: finalData,
			beforeSend: function() {
				$(".loader").show();
			}
		})
		.done(function(res) {
			if(res.status == 200) {
				$(".errFormMessage").addClass('text-success').html(res.message);
				$("#btnReset").trigger('click');
			}
			else {
				$(".errFormMessage").addClass('text-danger').html(res.message);
			}
		})
		.fail(function() {
			console.log("Failed to submit the form.");
		})
		.always(function() {
			$(".loader").hide();
		});
	}
});

function validateForm() {
	var name = $("#name").val();
	var address = $("#address").val();
	var zipcode = $("#zipcode").val();
	var mail = $("#email").val();
	var phone = $("#phone").val();
	var empid = $("#empid").val();
	var amount = $("#amount").val();
	var cause = $("#cause").val();
	var isIndian = changeCheckbox.checked;

	if($.trim(name).length <= 0) {
		var msg = "Name field cannot be left blank.";
		displayErrMessage($("#name"), msg);
		return false;
	}
	if($.trim(address).length <= 0) {
		var msg = "Address field cannot be left blank.";
		displayErrMessage($("#address"), msg);
		return false;
	}
	if(!/^[0-9]{6,6}$/.test(zipcode)) {
		var msg = "Kindly Enter 6 digit ZIP Code.";
		displayErrMessage($("#zipcode"), msg);
		return false;
	}
	if(!validateEmail(mail)) {
		var msg = "Kindly enter valid Email.";
		displayErrMessage($("#email"), msg);
		return false;
	}
	if(!/^[789][0-9]{9}$/.test(phone)) {
		if(phone.length != 10) {
			var msg = "Kindly Enter 10 digit mobile number.";
			displayErrMessage($("#phone"), msg);
			return false
		}
		var msg = "Phone number must start with 9, 8 or 7.";
		displayErrMessage($("#phone"), msg);
		return false;
	}
	if(!/^[A-Z]{2}[0-9]{7}$/.test(empid)) {
		if(empid.length != 9) {
			var msg = "Employee ID should contain exactly 9 characters.";
			displayErrMessage($("#empid"), msg);
			return false;
		}
		var msg = "Kindly Enter valid Employee ID. (E.g. FK1234567)";
		displayErrMessage($("#empid"), msg);
		return false;
	}
	if($.trim(amount).length <= 0) {
		var msg = "Amount field cannot be left blank.";
		displayErrMessage($("#amount"), msg);
		return false;
	}

	clearAllValidations();
	return true;

}

function displayErrMessage(element, message) {
	$(".form-control").removeClass('errInput');
	element.addClass('errInput');
	$(".errValidationMessage").html("");
	element.siblings('.errValidationMessage').html(message);
	element.focus();
}

function clearAllValidations() {
	$(".form-control").removeClass('errInput');
	$(".errValidationMessage").html("");
}

function validateEmail(mail)   
{  
 if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))  
  {  
    return (true)  
  }
  return (false)  
}