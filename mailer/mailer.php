<?php
require '../libs/PHPMailer-master/PHPMailerAutoload.php';

$mail = new PHPMailer;

extract($_POST);
$isIndian = ($isIndian == "true")?"Yes":"No";
$response = [];

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'kanishk.test2112@gmail.com';                 // SMTP username
$mail->Password = 'kanishktestaccount';                           // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 587;                                    // TCP port to connect to

$mail->setFrom('kanishk.test2112@gmail.com', 'Kanishk Test');
$mail->addAddress('kanishk2112@gmail.com', 'Kanishk Bansal');     // Add a recipient
$mail->addAddress('careers@giveindia.org');               // Name is optional
$mail->addReplyTo('kanishk2112@gmail.com', 'Kanishk Bansal');

$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'New Flipkart Employee Registration!';
$mail->Body    = '<b>Name:</b> '.$name.'<br />'.
				'<b>Address:</b> '.$address.'<br />'.
				'<b>ZIP Code:</b> '.$zipcode.'<br />'.
				'<b>Email Address:</b> '.$email.'<br />'.
				'<b>Phone Number:</b> '.$phone.'<br />'.
				'<b>Employee ID:</b> '.$empid.'<br />'.
				'<b>Amount:</b> '.$amount.'<br />'.
				'<b>Preferred Cause:</b> '.$cause.'<br />'.
				'<b>Indian Citizen:</b> '.$isIndian.'<br />'
				;
$mail->AltBody = 'New Flipkart Employee Registration!';

if(!$mail->send()) {
	$response = ["status"=>400, "message"=>"Employee Registration failed."];
	echo json_encode($response);
    // echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
	$response = ["status"=>200, "message"=>"Registration Details sent successfully."];
	echo json_encode($response);
}